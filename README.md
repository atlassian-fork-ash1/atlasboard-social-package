# Atlasboard social package

## Twitter module
You will need to set up these configuration parameters:

```
"twitter" : {
    "twitterTitle" : "Latest tweets",
    "twitterSearch" : "#zentyal",
    "numberOfTweets" : "5"
}
```

Also you have to set your Twitter credentials:
```
{
    "twitter" : {
        "consumer_key": "",
        "consumer_secret": "",
        "access_token": "",
        "access_token_secret": ""
    }
}
```