module.exports = function(config, dependencies, job_callback) {
  var logger = dependencies.logger;
  var error_message = "";

  /* Return error if no twitterSearch parameter is configured */
  if (!config.twitterSearch){
    error_message = 'no search parameter found';
    logger.error(error_message);

    return job_callback(error_message);
  }

  /* Return error if no auth parameters are configured */
  if (!config.globalAuth || !config.globalAuth.twitter.consumer_key ||
      !config.globalAuth.twitter.consumer_secret || !config.globalAuth.twitter.access_token ||
      !config.globalAuth.twitter.access_token_secret) {
    error_message = 'no authentication info available';
    logger.error(error_message);

    return job_callback(error_message);
  }

  var Twit = require('twit');

  /* Set credentials from globalAuth.json file */
  var T = new Twit({
    consumer_key: config.globalAuth.twitter.consumer_key,
    consumer_secret: config.globalAuth.twitter.consumer_secret,
    access_token: config.globalAuth.twitter.access_token,
    access_token_secret: config.globalAuth.twitter.access_token_secret
  });

  /* Set 5 as default number of tweets to fetch */
  number_of_tweets = config.numberOfTweets ? config.numberOfTweets : 5;
  search = encodeURIComponent(config.twitterSearch);

  /* Query the Twitter API */
  T.get('search/tweets', { q: search, count: number_of_tweets}, function(err, data, response) {
    if (err || !response || response.statusCode != 200) {
      error_message = err || (response ? ("bad statusCode: " + response.statusCode + " from " + options.url) : ("bad response from " + options.url));
      logger.error(error_message);
      job_callback(error_message);
    } else {
      job_callback(null, {feed: data['statuses'], title: config.twitterTitle});
    }
  });
};
